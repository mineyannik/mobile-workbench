package de.mineyannik.mobileworkbench;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class MobileWorkbench extends JavaPlugin
{
  public static Economy economy = null;

  @Override
  public void onEnable() { Config();
    getCommand("workbench").setExecutor(new Kommando(this));
    getCommand("wb").setExecutor(new Kommando(this));
    setupEconomy(); }

  @Override
  public void onDisable()
  {
  }

  public void Config() {
    this.reloadConfig();
      
    getConfig().set("price.workbench", 50);

    getConfig().addDefault("messages.notenoughmoney", "You don't have enough Money!");

    getConfig().addDefault("messages.nopermission", "You don't have Permissions to do that!");

    getConfig().addDefault("messages.success", "Here is your Workbench!");

    getConfig().addDefault("messages.payment", "You have paid 50 to open your Workbench!");

    getConfig().options().copyDefaults(true);
    saveConfig();
  }
  private boolean setupEconomy() {
    RegisteredServiceProvider economyProvider = getServer().getServicesManager().getRegistration(Economy.class);
    if (economyProvider != null) {
      economy = (Economy)economyProvider.getProvider();
    }

    return economy != null;
  }
}