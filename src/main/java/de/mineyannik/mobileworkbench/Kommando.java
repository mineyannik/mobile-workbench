package de.mineyannik.mobileworkbench;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Kommando
  implements CommandExecutor
{
  private final MobileWorkbench plugin;

  public Kommando(MobileWorkbench plugin)
  {
    this.plugin = plugin;
  }
  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage(ChatColor.RED + "That Command is only avialbe for Players!");
      return true;
    }
    Player p = (Player)sender;
    if (cmd.getName().equalsIgnoreCase("workbench")) {
      if (p.hasPermission("mobileworkbench.use.free")) {
        p.openWorkbench(null, true);
        p.sendMessage(ChatColor.DARK_AQUA + this.plugin.getConfig().getString("messages.success"));
        return true;
      }

      if (p.hasPermission("mobileworkbench.use")) {
        double Konto = MobileWorkbench.economy.getBalance(Bukkit.getOfflinePlayer(p.getUniqueId()));
        int preis = this.plugin.getConfig().getInt("price.workbench");
        if (Konto > preis) {
          MobileWorkbench.economy.withdrawPlayer(Bukkit.getOfflinePlayer(p.getUniqueId()), preis);
          p.sendMessage(ChatColor.YELLOW + this.plugin.getConfig().getString("messages.payment"));
          p.sendMessage(ChatColor.DARK_AQUA + this.plugin.getConfig().getString("messages.success"));
          p.openWorkbench(null, true);
          return true;
        }

        p.sendMessage(ChatColor.RED + this.plugin.getConfig().getString("messages.notenoughmoney"));
        return true;
      }

      p.sendMessage(ChatColor.DARK_RED + this.plugin.getConfig().getString("messages.nopermission"));
      return true;
    }

    if (cmd.getName().equalsIgnoreCase("wb")) {
      if (p.hasPermission("mobileworkbench.use.free")) {
        p.openWorkbench(null, true);
        p.sendMessage(ChatColor.DARK_AQUA + this.plugin.getConfig().getString("messages.success"));
        return true;
      }

      if (p.hasPermission("mobileworkbench.use")) {
        double Konto = MobileWorkbench.economy.getBalance(Bukkit.getOfflinePlayer(p.getUniqueId()));
        int preis = this.plugin.getConfig().getInt("price.workbench");
        if (Konto > preis) {
          MobileWorkbench.economy.withdrawPlayer(Bukkit.getOfflinePlayer(p.getUniqueId()), preis);

          p.sendMessage(ChatColor.DARK_AQUA + this.plugin.getConfig().getString("messages.success"));
          p.openWorkbench(null, true);
          return true;
        }

        p.sendMessage(ChatColor.RED + this.plugin.getConfig().getString("messages.notenoughmoney"));
        return true;
      }

      p.sendMessage(ChatColor.DARK_RED + this.plugin.getConfig().getString("messages.nopermission"));
      return true;
    }

    return false;
  }
}